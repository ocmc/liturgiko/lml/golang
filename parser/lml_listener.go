// Code generated from LML.g4 by ANTLR 4.13.1. DO NOT EDIT.

package parser // LML

import "github.com/antlr4-go/antlr/v4"

// LMLListener is a complete listener for a parse tree produced by LMLParser.
type LMLListener interface {
	antlr.ParseTreeListener

	// EnterTemplate is called when entering the template production.
	EnterTemplate(c *TemplateContext)

	// EnterTemplateMeta is called when entering the templateMeta production.
	EnterTemplateMeta(c *TemplateMetaContext)

	// EnterProperty is called when entering the property production.
	EnterProperty(c *PropertyContext)

	// EnterPdfProperty is called when entering the pdfProperty production.
	EnterPdfProperty(c *PdfPropertyContext)

	// EnterPdfSettingsBlock is called when entering the pdfSettingsBlock production.
	EnterPdfSettingsBlock(c *PdfSettingsBlockContext)

	// EnterPdfPrefaceBlock is called when entering the pdfPrefaceBlock production.
	EnterPdfPrefaceBlock(c *PdfPrefaceBlockContext)

	// EnterPropertyBlock is called when entering the propertyBlock production.
	EnterPropertyBlock(c *PropertyBlockContext)

	// EnterSection is called when entering the section production.
	EnterSection(c *SectionContext)

	// EnterSubsection is called when entering the subsection production.
	EnterSubsection(c *SubsectionContext)

	// EnterSubsubsection is called when entering the subsubsection production.
	EnterSubsubsection(c *SubsubsectionContext)

	// EnterStatement is called when entering the statement production.
	EnterStatement(c *StatementContext)

	// EnterIfStatement is called when entering the ifStatement production.
	EnterIfStatement(c *IfStatementContext)

	// EnterIfClause is called when entering the ifClause production.
	EnterIfClause(c *IfClauseContext)

	// EnterElseIfClause is called when entering the elseIfClause production.
	EnterElseIfClause(c *ElseIfClauseContext)

	// EnterElseClause is called when entering the elseClause production.
	EnterElseClause(c *ElseClauseContext)

	// EnterHtmlBlock is called when entering the htmlBlock production.
	EnterHtmlBlock(c *HtmlBlockContext)

	// EnterLdp is called when entering the ldp production.
	EnterLdp(c *LdpContext)

	// EnterPageBreak is called when entering the pageBreak production.
	EnterPageBreak(c *PageBreakContext)

	// EnterPdfBlock is called when entering the pdfBlock production.
	EnterPdfBlock(c *PdfBlockContext)

	// EnterRepeatBlock is called when entering the repeatBlock production.
	EnterRepeatBlock(c *RepeatBlockContext)

	// EnterRepeatTimes is called when entering the repeatTimes production.
	EnterRepeatTimes(c *RepeatTimesContext)

	// EnterRestoreDate is called when entering the restoreDate production.
	EnterRestoreDate(c *RestoreDateContext)

	// EnterRestoreMcDay is called when entering the restoreMcDay production.
	EnterRestoreMcDay(c *RestoreMcDayContext)

	// EnterRestoreRealm is called when entering the restoreRealm production.
	EnterRestoreRealm(c *RestoreRealmContext)

	// EnterRestoreVersion is called when entering the restoreVersion production.
	EnterRestoreVersion(c *RestoreVersionContext)

	// EnterSetMcDay is called when entering the setMcDay production.
	EnterSetMcDay(c *SetMcDayContext)

	// EnterSetRealm is called when entering the setRealm production.
	EnterSetRealm(c *SetRealmContext)

	// EnterSetVersion is called when entering the setVersion production.
	EnterSetVersion(c *SetVersionContext)

	// EnterSwitchStatement is called when entering the switchStatement production.
	EnterSwitchStatement(c *SwitchStatementContext)

	// EnterSwitchLabel is called when entering the switchLabel production.
	EnterSwitchLabel(c *SwitchLabelContext)

	// EnterDowName is called when entering the dowName production.
	EnterDowName(c *DowNameContext)

	// EnterInsert is called when entering the insert production.
	EnterInsert(c *InsertContext)

	// EnterAnchor is called when entering the anchor production.
	EnterAnchor(c *AnchorContext)

	// EnterHref is called when entering the href production.
	EnterHref(c *HrefContext)

	// EnterHrefId is called when entering the hrefId production.
	EnterHrefId(c *HrefIdContext)

	// EnterTarget is called when entering the target production.
	EnterTarget(c *TargetContext)

	// EnterTargetValue is called when entering the targetValue production.
	EnterTargetValue(c *TargetValueContext)

	// EnterAnchorLabel is called when entering the anchorLabel production.
	EnterAnchorLabel(c *AnchorLabelContext)

	// EnterAnchorLabelId is called when entering the anchorLabelId production.
	EnterAnchorLabelId(c *AnchorLabelIdContext)

	// EnterImage is called when entering the image production.
	EnterImage(c *ImageContext)

	// EnterSource is called when entering the source production.
	EnterSource(c *SourceContext)

	// EnterTitle is called when entering the title production.
	EnterTitle(c *TitleContext)

	// EnterWidth is called when entering the width production.
	EnterWidth(c *WidthContext)

	// EnterHeight is called when entering the height production.
	EnterHeight(c *HeightContext)

	// EnterH1 is called when entering the h1 production.
	EnterH1(c *H1Context)

	// EnterH2 is called when entering the h2 production.
	EnterH2(c *H2Context)

	// EnterH3 is called when entering the h3 production.
	EnterH3(c *H3Context)

	// EnterPara is called when entering the para production.
	EnterPara(c *ParaContext)

	// EnterPspan is called when entering the pspan production.
	EnterPspan(c *PspanContext)

	// EnterSpan is called when entering the span production.
	EnterSpan(c *SpanContext)

	// EnterMedia is called when entering the media production.
	EnterMedia(c *MediaContext)

	// EnterNid is called when entering the nid production.
	EnterNid(c *NidContext)

	// EnterLid is called when entering the lid production.
	EnterLid(c *LidContext)

	// EnterMonthName is called when entering the monthName production.
	EnterMonthName(c *MonthNameContext)

	// EnterLdpType is called when entering the ldpType production.
	EnterLdpType(c *LdpTypeContext)

	// EnterPosition is called when entering the position production.
	EnterPosition(c *PositionContext)

	// EnterPositionType is called when entering the positionType production.
	EnterPositionType(c *PositionTypeContext)

	// EnterDirective is called when entering the directive production.
	EnterDirective(c *DirectiveContext)

	// EnterLookup is called when entering the lookup production.
	EnterLookup(c *LookupContext)

	// EnterInsertDate is called when entering the insertDate production.
	EnterInsertDate(c *InsertDateContext)

	// EnterInsertH1 is called when entering the insertH1 production.
	EnterInsertH1(c *InsertH1Context)

	// EnterInsertH2 is called when entering the insertH2 production.
	EnterInsertH2(c *InsertH2Context)

	// EnterInsertH3 is called when entering the insertH3 production.
	EnterInsertH3(c *InsertH3Context)

	// EnterRid is called when entering the rid production.
	EnterRid(c *RidContext)

	// EnterOverride is called when entering the override production.
	EnterOverride(c *OverrideContext)

	// EnterOverrideMode is called when entering the overrideMode production.
	EnterOverrideMode(c *OverrideModeContext)

	// EnterOverrideDay is called when entering the overrideDay production.
	EnterOverrideDay(c *OverrideDayContext)

	// EnterBlankLine is called when entering the blankLine production.
	EnterBlankLine(c *BlankLineContext)

	// EnterHorizontalRule is called when entering the horizontalRule production.
	EnterHorizontalRule(c *HorizontalRuleContext)

	// EnterSid is called when entering the sid production.
	EnterSid(c *SidContext)

	// EnterTmplDate is called when entering the tmplDate production.
	EnterTmplDate(c *TmplDateContext)

	// EnterTmplCalendar is called when entering the tmplCalendar production.
	EnterTmplCalendar(c *TmplCalendarContext)

	// EnterTmplCss is called when entering the tmplCss production.
	EnterTmplCss(c *TmplCssContext)

	// EnterTmplDay is called when entering the tmplDay production.
	EnterTmplDay(c *TmplDayContext)

	// EnterTmplID is called when entering the tmplID production.
	EnterTmplID(c *TmplIDContext)

	// EnterTmplModel is called when entering the tmplModel production.
	EnterTmplModel(c *TmplModelContext)

	// EnterTmplMonth is called when entering the tmplMonth production.
	EnterTmplMonth(c *TmplMonthContext)

	// EnterTmplOutput is called when entering the tmplOutput production.
	EnterTmplOutput(c *TmplOutputContext)

	// EnterTmplPageHeaderEven is called when entering the tmplPageHeaderEven production.
	EnterTmplPageHeaderEven(c *TmplPageHeaderEvenContext)

	// EnterTmplPageFooterEven is called when entering the tmplPageFooterEven production.
	EnterTmplPageFooterEven(c *TmplPageFooterEvenContext)

	// EnterTmplPageHeaderFirst is called when entering the tmplPageHeaderFirst production.
	EnterTmplPageHeaderFirst(c *TmplPageHeaderFirstContext)

	// EnterTmplPageFooterFirst is called when entering the tmplPageFooterFirst production.
	EnterTmplPageFooterFirst(c *TmplPageFooterFirstContext)

	// EnterTmplPageHeaderOdd is called when entering the tmplPageHeaderOdd production.
	EnterTmplPageHeaderOdd(c *TmplPageHeaderOddContext)

	// EnterTmplPageFooterOdd is called when entering the tmplPageFooterOdd production.
	EnterTmplPageFooterOdd(c *TmplPageFooterOddContext)

	// EnterTmplPageHeader is called when entering the tmplPageHeader production.
	EnterTmplPageHeader(c *TmplPageHeaderContext)

	// EnterTmplPageFooter is called when entering the tmplPageFooter production.
	EnterTmplPageFooter(c *TmplPageFooterContext)

	// EnterTmplPageNumber is called when entering the tmplPageNumber production.
	EnterTmplPageNumber(c *TmplPageNumberContext)

	// EnterTmplStatus is called when entering the tmplStatus production.
	EnterTmplStatus(c *TmplStatusContext)

	// EnterTmplIndexLastTitleOverride is called when entering the tmplIndexLastTitleOverride production.
	EnterTmplIndexLastTitleOverride(c *TmplIndexLastTitleOverrideContext)

	// EnterTmplTitleCodes is called when entering the tmplTitleCodes production.
	EnterTmplTitleCodes(c *TmplTitleCodesContext)

	// EnterTmplType is called when entering the tmplType production.
	EnterTmplType(c *TmplTypeContext)

	// EnterTmplOffice is called when entering the tmplOffice production.
	EnterTmplOffice(c *TmplOfficeContext)

	// EnterTmplYear is called when entering the tmplYear production.
	EnterTmplYear(c *TmplYearContext)

	// EnterBlock is called when entering the block production.
	EnterBlock(c *BlockContext)

	// EnterCaseStatement is called when entering the caseStatement production.
	EnterCaseStatement(c *CaseStatementContext)

	// EnterSwitchDefault is called when entering the switchDefault production.
	EnterSwitchDefault(c *SwitchDefaultContext)

	// EnterCaseExpression is called when entering the caseExpression production.
	EnterCaseExpression(c *CaseExpressionContext)

	// EnterIntegerExpression is called when entering the integerExpression production.
	EnterIntegerExpression(c *IntegerExpressionContext)

	// EnterIntegerList is called when entering the integerList production.
	EnterIntegerList(c *IntegerListContext)

	// EnterDowExpression is called when entering the dowExpression production.
	EnterDowExpression(c *DowExpressionContext)

	// EnterRidExpression is called when entering the ridExpression production.
	EnterRidExpression(c *RidExpressionContext)

	// EnterDowList is called when entering the dowList production.
	EnterDowList(c *DowListContext)

	// EnterMonthDay is called when entering the monthDay production.
	EnterMonthDay(c *MonthDayContext)

	// EnterOperator is called when entering the operator production.
	EnterOperator(c *OperatorContext)

	// EnterExpression is called when entering the expression production.
	EnterExpression(c *ExpressionContext)

	// ExitTemplate is called when exiting the template production.
	ExitTemplate(c *TemplateContext)

	// ExitTemplateMeta is called when exiting the templateMeta production.
	ExitTemplateMeta(c *TemplateMetaContext)

	// ExitProperty is called when exiting the property production.
	ExitProperty(c *PropertyContext)

	// ExitPdfProperty is called when exiting the pdfProperty production.
	ExitPdfProperty(c *PdfPropertyContext)

	// ExitPdfSettingsBlock is called when exiting the pdfSettingsBlock production.
	ExitPdfSettingsBlock(c *PdfSettingsBlockContext)

	// ExitPdfPrefaceBlock is called when exiting the pdfPrefaceBlock production.
	ExitPdfPrefaceBlock(c *PdfPrefaceBlockContext)

	// ExitPropertyBlock is called when exiting the propertyBlock production.
	ExitPropertyBlock(c *PropertyBlockContext)

	// ExitSection is called when exiting the section production.
	ExitSection(c *SectionContext)

	// ExitSubsection is called when exiting the subsection production.
	ExitSubsection(c *SubsectionContext)

	// ExitSubsubsection is called when exiting the subsubsection production.
	ExitSubsubsection(c *SubsubsectionContext)

	// ExitStatement is called when exiting the statement production.
	ExitStatement(c *StatementContext)

	// ExitIfStatement is called when exiting the ifStatement production.
	ExitIfStatement(c *IfStatementContext)

	// ExitIfClause is called when exiting the ifClause production.
	ExitIfClause(c *IfClauseContext)

	// ExitElseIfClause is called when exiting the elseIfClause production.
	ExitElseIfClause(c *ElseIfClauseContext)

	// ExitElseClause is called when exiting the elseClause production.
	ExitElseClause(c *ElseClauseContext)

	// ExitHtmlBlock is called when exiting the htmlBlock production.
	ExitHtmlBlock(c *HtmlBlockContext)

	// ExitLdp is called when exiting the ldp production.
	ExitLdp(c *LdpContext)

	// ExitPageBreak is called when exiting the pageBreak production.
	ExitPageBreak(c *PageBreakContext)

	// ExitPdfBlock is called when exiting the pdfBlock production.
	ExitPdfBlock(c *PdfBlockContext)

	// ExitRepeatBlock is called when exiting the repeatBlock production.
	ExitRepeatBlock(c *RepeatBlockContext)

	// ExitRepeatTimes is called when exiting the repeatTimes production.
	ExitRepeatTimes(c *RepeatTimesContext)

	// ExitRestoreDate is called when exiting the restoreDate production.
	ExitRestoreDate(c *RestoreDateContext)

	// ExitRestoreMcDay is called when exiting the restoreMcDay production.
	ExitRestoreMcDay(c *RestoreMcDayContext)

	// ExitRestoreRealm is called when exiting the restoreRealm production.
	ExitRestoreRealm(c *RestoreRealmContext)

	// ExitRestoreVersion is called when exiting the restoreVersion production.
	ExitRestoreVersion(c *RestoreVersionContext)

	// ExitSetMcDay is called when exiting the setMcDay production.
	ExitSetMcDay(c *SetMcDayContext)

	// ExitSetRealm is called when exiting the setRealm production.
	ExitSetRealm(c *SetRealmContext)

	// ExitSetVersion is called when exiting the setVersion production.
	ExitSetVersion(c *SetVersionContext)

	// ExitSwitchStatement is called when exiting the switchStatement production.
	ExitSwitchStatement(c *SwitchStatementContext)

	// ExitSwitchLabel is called when exiting the switchLabel production.
	ExitSwitchLabel(c *SwitchLabelContext)

	// ExitDowName is called when exiting the dowName production.
	ExitDowName(c *DowNameContext)

	// ExitInsert is called when exiting the insert production.
	ExitInsert(c *InsertContext)

	// ExitAnchor is called when exiting the anchor production.
	ExitAnchor(c *AnchorContext)

	// ExitHref is called when exiting the href production.
	ExitHref(c *HrefContext)

	// ExitHrefId is called when exiting the hrefId production.
	ExitHrefId(c *HrefIdContext)

	// ExitTarget is called when exiting the target production.
	ExitTarget(c *TargetContext)

	// ExitTargetValue is called when exiting the targetValue production.
	ExitTargetValue(c *TargetValueContext)

	// ExitAnchorLabel is called when exiting the anchorLabel production.
	ExitAnchorLabel(c *AnchorLabelContext)

	// ExitAnchorLabelId is called when exiting the anchorLabelId production.
	ExitAnchorLabelId(c *AnchorLabelIdContext)

	// ExitImage is called when exiting the image production.
	ExitImage(c *ImageContext)

	// ExitSource is called when exiting the source production.
	ExitSource(c *SourceContext)

	// ExitTitle is called when exiting the title production.
	ExitTitle(c *TitleContext)

	// ExitWidth is called when exiting the width production.
	ExitWidth(c *WidthContext)

	// ExitHeight is called when exiting the height production.
	ExitHeight(c *HeightContext)

	// ExitH1 is called when exiting the h1 production.
	ExitH1(c *H1Context)

	// ExitH2 is called when exiting the h2 production.
	ExitH2(c *H2Context)

	// ExitH3 is called when exiting the h3 production.
	ExitH3(c *H3Context)

	// ExitPara is called when exiting the para production.
	ExitPara(c *ParaContext)

	// ExitPspan is called when exiting the pspan production.
	ExitPspan(c *PspanContext)

	// ExitSpan is called when exiting the span production.
	ExitSpan(c *SpanContext)

	// ExitMedia is called when exiting the media production.
	ExitMedia(c *MediaContext)

	// ExitNid is called when exiting the nid production.
	ExitNid(c *NidContext)

	// ExitLid is called when exiting the lid production.
	ExitLid(c *LidContext)

	// ExitMonthName is called when exiting the monthName production.
	ExitMonthName(c *MonthNameContext)

	// ExitLdpType is called when exiting the ldpType production.
	ExitLdpType(c *LdpTypeContext)

	// ExitPosition is called when exiting the position production.
	ExitPosition(c *PositionContext)

	// ExitPositionType is called when exiting the positionType production.
	ExitPositionType(c *PositionTypeContext)

	// ExitDirective is called when exiting the directive production.
	ExitDirective(c *DirectiveContext)

	// ExitLookup is called when exiting the lookup production.
	ExitLookup(c *LookupContext)

	// ExitInsertDate is called when exiting the insertDate production.
	ExitInsertDate(c *InsertDateContext)

	// ExitInsertH1 is called when exiting the insertH1 production.
	ExitInsertH1(c *InsertH1Context)

	// ExitInsertH2 is called when exiting the insertH2 production.
	ExitInsertH2(c *InsertH2Context)

	// ExitInsertH3 is called when exiting the insertH3 production.
	ExitInsertH3(c *InsertH3Context)

	// ExitRid is called when exiting the rid production.
	ExitRid(c *RidContext)

	// ExitOverride is called when exiting the override production.
	ExitOverride(c *OverrideContext)

	// ExitOverrideMode is called when exiting the overrideMode production.
	ExitOverrideMode(c *OverrideModeContext)

	// ExitOverrideDay is called when exiting the overrideDay production.
	ExitOverrideDay(c *OverrideDayContext)

	// ExitBlankLine is called when exiting the blankLine production.
	ExitBlankLine(c *BlankLineContext)

	// ExitHorizontalRule is called when exiting the horizontalRule production.
	ExitHorizontalRule(c *HorizontalRuleContext)

	// ExitSid is called when exiting the sid production.
	ExitSid(c *SidContext)

	// ExitTmplDate is called when exiting the tmplDate production.
	ExitTmplDate(c *TmplDateContext)

	// ExitTmplCalendar is called when exiting the tmplCalendar production.
	ExitTmplCalendar(c *TmplCalendarContext)

	// ExitTmplCss is called when exiting the tmplCss production.
	ExitTmplCss(c *TmplCssContext)

	// ExitTmplDay is called when exiting the tmplDay production.
	ExitTmplDay(c *TmplDayContext)

	// ExitTmplID is called when exiting the tmplID production.
	ExitTmplID(c *TmplIDContext)

	// ExitTmplModel is called when exiting the tmplModel production.
	ExitTmplModel(c *TmplModelContext)

	// ExitTmplMonth is called when exiting the tmplMonth production.
	ExitTmplMonth(c *TmplMonthContext)

	// ExitTmplOutput is called when exiting the tmplOutput production.
	ExitTmplOutput(c *TmplOutputContext)

	// ExitTmplPageHeaderEven is called when exiting the tmplPageHeaderEven production.
	ExitTmplPageHeaderEven(c *TmplPageHeaderEvenContext)

	// ExitTmplPageFooterEven is called when exiting the tmplPageFooterEven production.
	ExitTmplPageFooterEven(c *TmplPageFooterEvenContext)

	// ExitTmplPageHeaderFirst is called when exiting the tmplPageHeaderFirst production.
	ExitTmplPageHeaderFirst(c *TmplPageHeaderFirstContext)

	// ExitTmplPageFooterFirst is called when exiting the tmplPageFooterFirst production.
	ExitTmplPageFooterFirst(c *TmplPageFooterFirstContext)

	// ExitTmplPageHeaderOdd is called when exiting the tmplPageHeaderOdd production.
	ExitTmplPageHeaderOdd(c *TmplPageHeaderOddContext)

	// ExitTmplPageFooterOdd is called when exiting the tmplPageFooterOdd production.
	ExitTmplPageFooterOdd(c *TmplPageFooterOddContext)

	// ExitTmplPageHeader is called when exiting the tmplPageHeader production.
	ExitTmplPageHeader(c *TmplPageHeaderContext)

	// ExitTmplPageFooter is called when exiting the tmplPageFooter production.
	ExitTmplPageFooter(c *TmplPageFooterContext)

	// ExitTmplPageNumber is called when exiting the tmplPageNumber production.
	ExitTmplPageNumber(c *TmplPageNumberContext)

	// ExitTmplStatus is called when exiting the tmplStatus production.
	ExitTmplStatus(c *TmplStatusContext)

	// ExitTmplIndexLastTitleOverride is called when exiting the tmplIndexLastTitleOverride production.
	ExitTmplIndexLastTitleOverride(c *TmplIndexLastTitleOverrideContext)

	// ExitTmplTitleCodes is called when exiting the tmplTitleCodes production.
	ExitTmplTitleCodes(c *TmplTitleCodesContext)

	// ExitTmplType is called when exiting the tmplType production.
	ExitTmplType(c *TmplTypeContext)

	// ExitTmplOffice is called when exiting the tmplOffice production.
	ExitTmplOffice(c *TmplOfficeContext)

	// ExitTmplYear is called when exiting the tmplYear production.
	ExitTmplYear(c *TmplYearContext)

	// ExitBlock is called when exiting the block production.
	ExitBlock(c *BlockContext)

	// ExitCaseStatement is called when exiting the caseStatement production.
	ExitCaseStatement(c *CaseStatementContext)

	// ExitSwitchDefault is called when exiting the switchDefault production.
	ExitSwitchDefault(c *SwitchDefaultContext)

	// ExitCaseExpression is called when exiting the caseExpression production.
	ExitCaseExpression(c *CaseExpressionContext)

	// ExitIntegerExpression is called when exiting the integerExpression production.
	ExitIntegerExpression(c *IntegerExpressionContext)

	// ExitIntegerList is called when exiting the integerList production.
	ExitIntegerList(c *IntegerListContext)

	// ExitDowExpression is called when exiting the dowExpression production.
	ExitDowExpression(c *DowExpressionContext)

	// ExitRidExpression is called when exiting the ridExpression production.
	ExitRidExpression(c *RidExpressionContext)

	// ExitDowList is called when exiting the dowList production.
	ExitDowList(c *DowListContext)

	// ExitMonthDay is called when exiting the monthDay production.
	ExitMonthDay(c *MonthDayContext)

	// ExitOperator is called when exiting the operator production.
	ExitOperator(c *OperatorContext)

	// ExitExpression is called when exiting the expression production.
	ExitExpression(c *ExpressionContext)
}
