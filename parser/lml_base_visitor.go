// Code generated from LML.g4 by ANTLR 4.13.1. DO NOT EDIT.

package parser // LML

import "github.com/antlr4-go/antlr/v4"

type BaseLMLVisitor struct {
	*antlr.BaseParseTreeVisitor
}

func (v *BaseLMLVisitor) VisitTemplate(ctx *TemplateContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTemplateMeta(ctx *TemplateMetaContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitProperty(ctx *PropertyContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitPdfProperty(ctx *PdfPropertyContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitPdfSettingsBlock(ctx *PdfSettingsBlockContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitPdfPrefaceBlock(ctx *PdfPrefaceBlockContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitPropertyBlock(ctx *PropertyBlockContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitSection(ctx *SectionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitSubsection(ctx *SubsectionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitSubsubsection(ctx *SubsubsectionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitStatement(ctx *StatementContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitIfStatement(ctx *IfStatementContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitIfClause(ctx *IfClauseContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitElseIfClause(ctx *ElseIfClauseContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitElseClause(ctx *ElseClauseContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitHtmlBlock(ctx *HtmlBlockContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitLdp(ctx *LdpContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitPageBreak(ctx *PageBreakContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitPdfBlock(ctx *PdfBlockContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitRepeatBlock(ctx *RepeatBlockContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitRepeatTimes(ctx *RepeatTimesContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitRestoreDate(ctx *RestoreDateContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitRestoreMcDay(ctx *RestoreMcDayContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitRestoreRealm(ctx *RestoreRealmContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitRestoreVersion(ctx *RestoreVersionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitSetMcDay(ctx *SetMcDayContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitSetRealm(ctx *SetRealmContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitSetVersion(ctx *SetVersionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitSwitchStatement(ctx *SwitchStatementContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitSwitchLabel(ctx *SwitchLabelContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitDowName(ctx *DowNameContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitInsert(ctx *InsertContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitAnchor(ctx *AnchorContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitHref(ctx *HrefContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitHrefId(ctx *HrefIdContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTarget(ctx *TargetContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTargetValue(ctx *TargetValueContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitAnchorLabel(ctx *AnchorLabelContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitAnchorLabelId(ctx *AnchorLabelIdContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitImage(ctx *ImageContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitSource(ctx *SourceContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTitle(ctx *TitleContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitWidth(ctx *WidthContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitHeight(ctx *HeightContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitH1(ctx *H1Context) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitH2(ctx *H2Context) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitH3(ctx *H3Context) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitPara(ctx *ParaContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitPspan(ctx *PspanContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitSpan(ctx *SpanContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitMedia(ctx *MediaContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitNid(ctx *NidContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitLid(ctx *LidContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitMonthName(ctx *MonthNameContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitLdpType(ctx *LdpTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitPosition(ctx *PositionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitPositionType(ctx *PositionTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitDirective(ctx *DirectiveContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitLookup(ctx *LookupContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitInsertDate(ctx *InsertDateContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitInsertH1(ctx *InsertH1Context) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitInsertH2(ctx *InsertH2Context) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitInsertH3(ctx *InsertH3Context) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitRid(ctx *RidContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitOverride(ctx *OverrideContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitOverrideMode(ctx *OverrideModeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitOverrideDay(ctx *OverrideDayContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitBlankLine(ctx *BlankLineContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitHorizontalRule(ctx *HorizontalRuleContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitSid(ctx *SidContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplDate(ctx *TmplDateContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplCalendar(ctx *TmplCalendarContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplCss(ctx *TmplCssContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplDay(ctx *TmplDayContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplID(ctx *TmplIDContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplModel(ctx *TmplModelContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplMonth(ctx *TmplMonthContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplOutput(ctx *TmplOutputContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplPageHeaderEven(ctx *TmplPageHeaderEvenContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplPageFooterEven(ctx *TmplPageFooterEvenContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplPageHeaderFirst(ctx *TmplPageHeaderFirstContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplPageFooterFirst(ctx *TmplPageFooterFirstContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplPageHeaderOdd(ctx *TmplPageHeaderOddContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplPageFooterOdd(ctx *TmplPageFooterOddContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplPageHeader(ctx *TmplPageHeaderContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplPageFooter(ctx *TmplPageFooterContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplPageNumber(ctx *TmplPageNumberContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplStatus(ctx *TmplStatusContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplIndexLastTitleOverride(ctx *TmplIndexLastTitleOverrideContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplTitleCodes(ctx *TmplTitleCodesContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplType(ctx *TmplTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplOffice(ctx *TmplOfficeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitTmplYear(ctx *TmplYearContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitBlock(ctx *BlockContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitCaseStatement(ctx *CaseStatementContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitSwitchDefault(ctx *SwitchDefaultContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitCaseExpression(ctx *CaseExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitIntegerExpression(ctx *IntegerExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitIntegerList(ctx *IntegerListContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitDowExpression(ctx *DowExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitRidExpression(ctx *RidExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitDowList(ctx *DowListContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitMonthDay(ctx *MonthDayContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitOperator(ctx *OperatorContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLMLVisitor) VisitExpression(ctx *ExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}
