// Code generated from LML.g4 by ANTLR 4.13.1. DO NOT EDIT.

package parser // LML

import "github.com/antlr4-go/antlr/v4"

// A complete Visitor for a parse tree produced by LMLParser.
type LMLVisitor interface {
	antlr.ParseTreeVisitor

	// Visit a parse tree produced by LMLParser#template.
	VisitTemplate(ctx *TemplateContext) interface{}

	// Visit a parse tree produced by LMLParser#templateMeta.
	VisitTemplateMeta(ctx *TemplateMetaContext) interface{}

	// Visit a parse tree produced by LMLParser#property.
	VisitProperty(ctx *PropertyContext) interface{}

	// Visit a parse tree produced by LMLParser#pdfProperty.
	VisitPdfProperty(ctx *PdfPropertyContext) interface{}

	// Visit a parse tree produced by LMLParser#pdfSettingsBlock.
	VisitPdfSettingsBlock(ctx *PdfSettingsBlockContext) interface{}

	// Visit a parse tree produced by LMLParser#pdfPrefaceBlock.
	VisitPdfPrefaceBlock(ctx *PdfPrefaceBlockContext) interface{}

	// Visit a parse tree produced by LMLParser#propertyBlock.
	VisitPropertyBlock(ctx *PropertyBlockContext) interface{}

	// Visit a parse tree produced by LMLParser#section.
	VisitSection(ctx *SectionContext) interface{}

	// Visit a parse tree produced by LMLParser#subsection.
	VisitSubsection(ctx *SubsectionContext) interface{}

	// Visit a parse tree produced by LMLParser#subsubsection.
	VisitSubsubsection(ctx *SubsubsectionContext) interface{}

	// Visit a parse tree produced by LMLParser#statement.
	VisitStatement(ctx *StatementContext) interface{}

	// Visit a parse tree produced by LMLParser#ifStatement.
	VisitIfStatement(ctx *IfStatementContext) interface{}

	// Visit a parse tree produced by LMLParser#ifClause.
	VisitIfClause(ctx *IfClauseContext) interface{}

	// Visit a parse tree produced by LMLParser#elseIfClause.
	VisitElseIfClause(ctx *ElseIfClauseContext) interface{}

	// Visit a parse tree produced by LMLParser#elseClause.
	VisitElseClause(ctx *ElseClauseContext) interface{}

	// Visit a parse tree produced by LMLParser#htmlBlock.
	VisitHtmlBlock(ctx *HtmlBlockContext) interface{}

	// Visit a parse tree produced by LMLParser#ldp.
	VisitLdp(ctx *LdpContext) interface{}

	// Visit a parse tree produced by LMLParser#pageBreak.
	VisitPageBreak(ctx *PageBreakContext) interface{}

	// Visit a parse tree produced by LMLParser#pdfBlock.
	VisitPdfBlock(ctx *PdfBlockContext) interface{}

	// Visit a parse tree produced by LMLParser#repeatBlock.
	VisitRepeatBlock(ctx *RepeatBlockContext) interface{}

	// Visit a parse tree produced by LMLParser#repeatTimes.
	VisitRepeatTimes(ctx *RepeatTimesContext) interface{}

	// Visit a parse tree produced by LMLParser#restoreDate.
	VisitRestoreDate(ctx *RestoreDateContext) interface{}

	// Visit a parse tree produced by LMLParser#restoreMcDay.
	VisitRestoreMcDay(ctx *RestoreMcDayContext) interface{}

	// Visit a parse tree produced by LMLParser#restoreRealm.
	VisitRestoreRealm(ctx *RestoreRealmContext) interface{}

	// Visit a parse tree produced by LMLParser#restoreVersion.
	VisitRestoreVersion(ctx *RestoreVersionContext) interface{}

	// Visit a parse tree produced by LMLParser#setMcDay.
	VisitSetMcDay(ctx *SetMcDayContext) interface{}

	// Visit a parse tree produced by LMLParser#setRealm.
	VisitSetRealm(ctx *SetRealmContext) interface{}

	// Visit a parse tree produced by LMLParser#setVersion.
	VisitSetVersion(ctx *SetVersionContext) interface{}

	// Visit a parse tree produced by LMLParser#switchStatement.
	VisitSwitchStatement(ctx *SwitchStatementContext) interface{}

	// Visit a parse tree produced by LMLParser#switchLabel.
	VisitSwitchLabel(ctx *SwitchLabelContext) interface{}

	// Visit a parse tree produced by LMLParser#dowName.
	VisitDowName(ctx *DowNameContext) interface{}

	// Visit a parse tree produced by LMLParser#insert.
	VisitInsert(ctx *InsertContext) interface{}

	// Visit a parse tree produced by LMLParser#anchor.
	VisitAnchor(ctx *AnchorContext) interface{}

	// Visit a parse tree produced by LMLParser#href.
	VisitHref(ctx *HrefContext) interface{}

	// Visit a parse tree produced by LMLParser#hrefId.
	VisitHrefId(ctx *HrefIdContext) interface{}

	// Visit a parse tree produced by LMLParser#target.
	VisitTarget(ctx *TargetContext) interface{}

	// Visit a parse tree produced by LMLParser#targetValue.
	VisitTargetValue(ctx *TargetValueContext) interface{}

	// Visit a parse tree produced by LMLParser#anchorLabel.
	VisitAnchorLabel(ctx *AnchorLabelContext) interface{}

	// Visit a parse tree produced by LMLParser#anchorLabelId.
	VisitAnchorLabelId(ctx *AnchorLabelIdContext) interface{}

	// Visit a parse tree produced by LMLParser#image.
	VisitImage(ctx *ImageContext) interface{}

	// Visit a parse tree produced by LMLParser#source.
	VisitSource(ctx *SourceContext) interface{}

	// Visit a parse tree produced by LMLParser#title.
	VisitTitle(ctx *TitleContext) interface{}

	// Visit a parse tree produced by LMLParser#width.
	VisitWidth(ctx *WidthContext) interface{}

	// Visit a parse tree produced by LMLParser#height.
	VisitHeight(ctx *HeightContext) interface{}

	// Visit a parse tree produced by LMLParser#h1.
	VisitH1(ctx *H1Context) interface{}

	// Visit a parse tree produced by LMLParser#h2.
	VisitH2(ctx *H2Context) interface{}

	// Visit a parse tree produced by LMLParser#h3.
	VisitH3(ctx *H3Context) interface{}

	// Visit a parse tree produced by LMLParser#para.
	VisitPara(ctx *ParaContext) interface{}

	// Visit a parse tree produced by LMLParser#pspan.
	VisitPspan(ctx *PspanContext) interface{}

	// Visit a parse tree produced by LMLParser#span.
	VisitSpan(ctx *SpanContext) interface{}

	// Visit a parse tree produced by LMLParser#media.
	VisitMedia(ctx *MediaContext) interface{}

	// Visit a parse tree produced by LMLParser#nid.
	VisitNid(ctx *NidContext) interface{}

	// Visit a parse tree produced by LMLParser#lid.
	VisitLid(ctx *LidContext) interface{}

	// Visit a parse tree produced by LMLParser#monthName.
	VisitMonthName(ctx *MonthNameContext) interface{}

	// Visit a parse tree produced by LMLParser#ldpType.
	VisitLdpType(ctx *LdpTypeContext) interface{}

	// Visit a parse tree produced by LMLParser#position.
	VisitPosition(ctx *PositionContext) interface{}

	// Visit a parse tree produced by LMLParser#positionType.
	VisitPositionType(ctx *PositionTypeContext) interface{}

	// Visit a parse tree produced by LMLParser#directive.
	VisitDirective(ctx *DirectiveContext) interface{}

	// Visit a parse tree produced by LMLParser#lookup.
	VisitLookup(ctx *LookupContext) interface{}

	// Visit a parse tree produced by LMLParser#insertDate.
	VisitInsertDate(ctx *InsertDateContext) interface{}

	// Visit a parse tree produced by LMLParser#insertH1.
	VisitInsertH1(ctx *InsertH1Context) interface{}

	// Visit a parse tree produced by LMLParser#insertH2.
	VisitInsertH2(ctx *InsertH2Context) interface{}

	// Visit a parse tree produced by LMLParser#insertH3.
	VisitInsertH3(ctx *InsertH3Context) interface{}

	// Visit a parse tree produced by LMLParser#rid.
	VisitRid(ctx *RidContext) interface{}

	// Visit a parse tree produced by LMLParser#override.
	VisitOverride(ctx *OverrideContext) interface{}

	// Visit a parse tree produced by LMLParser#overrideMode.
	VisitOverrideMode(ctx *OverrideModeContext) interface{}

	// Visit a parse tree produced by LMLParser#overrideDay.
	VisitOverrideDay(ctx *OverrideDayContext) interface{}

	// Visit a parse tree produced by LMLParser#blankLine.
	VisitBlankLine(ctx *BlankLineContext) interface{}

	// Visit a parse tree produced by LMLParser#horizontalRule.
	VisitHorizontalRule(ctx *HorizontalRuleContext) interface{}

	// Visit a parse tree produced by LMLParser#sid.
	VisitSid(ctx *SidContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplDate.
	VisitTmplDate(ctx *TmplDateContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplCalendar.
	VisitTmplCalendar(ctx *TmplCalendarContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplCss.
	VisitTmplCss(ctx *TmplCssContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplDay.
	VisitTmplDay(ctx *TmplDayContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplID.
	VisitTmplID(ctx *TmplIDContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplModel.
	VisitTmplModel(ctx *TmplModelContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplMonth.
	VisitTmplMonth(ctx *TmplMonthContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplOutput.
	VisitTmplOutput(ctx *TmplOutputContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplPageHeaderEven.
	VisitTmplPageHeaderEven(ctx *TmplPageHeaderEvenContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplPageFooterEven.
	VisitTmplPageFooterEven(ctx *TmplPageFooterEvenContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplPageHeaderFirst.
	VisitTmplPageHeaderFirst(ctx *TmplPageHeaderFirstContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplPageFooterFirst.
	VisitTmplPageFooterFirst(ctx *TmplPageFooterFirstContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplPageHeaderOdd.
	VisitTmplPageHeaderOdd(ctx *TmplPageHeaderOddContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplPageFooterOdd.
	VisitTmplPageFooterOdd(ctx *TmplPageFooterOddContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplPageHeader.
	VisitTmplPageHeader(ctx *TmplPageHeaderContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplPageFooter.
	VisitTmplPageFooter(ctx *TmplPageFooterContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplPageNumber.
	VisitTmplPageNumber(ctx *TmplPageNumberContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplStatus.
	VisitTmplStatus(ctx *TmplStatusContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplIndexLastTitleOverride.
	VisitTmplIndexLastTitleOverride(ctx *TmplIndexLastTitleOverrideContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplTitleCodes.
	VisitTmplTitleCodes(ctx *TmplTitleCodesContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplType.
	VisitTmplType(ctx *TmplTypeContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplOffice.
	VisitTmplOffice(ctx *TmplOfficeContext) interface{}

	// Visit a parse tree produced by LMLParser#tmplYear.
	VisitTmplYear(ctx *TmplYearContext) interface{}

	// Visit a parse tree produced by LMLParser#block.
	VisitBlock(ctx *BlockContext) interface{}

	// Visit a parse tree produced by LMLParser#caseStatement.
	VisitCaseStatement(ctx *CaseStatementContext) interface{}

	// Visit a parse tree produced by LMLParser#switchDefault.
	VisitSwitchDefault(ctx *SwitchDefaultContext) interface{}

	// Visit a parse tree produced by LMLParser#caseExpression.
	VisitCaseExpression(ctx *CaseExpressionContext) interface{}

	// Visit a parse tree produced by LMLParser#integerExpression.
	VisitIntegerExpression(ctx *IntegerExpressionContext) interface{}

	// Visit a parse tree produced by LMLParser#integerList.
	VisitIntegerList(ctx *IntegerListContext) interface{}

	// Visit a parse tree produced by LMLParser#dowExpression.
	VisitDowExpression(ctx *DowExpressionContext) interface{}

	// Visit a parse tree produced by LMLParser#ridExpression.
	VisitRidExpression(ctx *RidExpressionContext) interface{}

	// Visit a parse tree produced by LMLParser#dowList.
	VisitDowList(ctx *DowListContext) interface{}

	// Visit a parse tree produced by LMLParser#monthDay.
	VisitMonthDay(ctx *MonthDayContext) interface{}

	// Visit a parse tree produced by LMLParser#operator.
	VisitOperator(ctx *OperatorContext) interface{}

	// Visit a parse tree produced by LMLParser#expression.
	VisitExpression(ctx *ExpressionContext) interface{}
}
