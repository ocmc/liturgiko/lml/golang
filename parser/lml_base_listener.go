// Code generated from LML.g4 by ANTLR 4.13.1. DO NOT EDIT.

package parser // LML

import "github.com/antlr4-go/antlr/v4"

// BaseLMLListener is a complete listener for a parse tree produced by LMLParser.
type BaseLMLListener struct{}

var _ LMLListener = &BaseLMLListener{}

// VisitTerminal is called when a terminal node is visited.
func (s *BaseLMLListener) VisitTerminal(node antlr.TerminalNode) {}

// VisitErrorNode is called when an error node is visited.
func (s *BaseLMLListener) VisitErrorNode(node antlr.ErrorNode) {}

// EnterEveryRule is called when any rule is entered.
func (s *BaseLMLListener) EnterEveryRule(ctx antlr.ParserRuleContext) {}

// ExitEveryRule is called when any rule is exited.
func (s *BaseLMLListener) ExitEveryRule(ctx antlr.ParserRuleContext) {}

// EnterTemplate is called when production template is entered.
func (s *BaseLMLListener) EnterTemplate(ctx *TemplateContext) {}

// ExitTemplate is called when production template is exited.
func (s *BaseLMLListener) ExitTemplate(ctx *TemplateContext) {}

// EnterTemplateMeta is called when production templateMeta is entered.
func (s *BaseLMLListener) EnterTemplateMeta(ctx *TemplateMetaContext) {}

// ExitTemplateMeta is called when production templateMeta is exited.
func (s *BaseLMLListener) ExitTemplateMeta(ctx *TemplateMetaContext) {}

// EnterProperty is called when production property is entered.
func (s *BaseLMLListener) EnterProperty(ctx *PropertyContext) {}

// ExitProperty is called when production property is exited.
func (s *BaseLMLListener) ExitProperty(ctx *PropertyContext) {}

// EnterPdfProperty is called when production pdfProperty is entered.
func (s *BaseLMLListener) EnterPdfProperty(ctx *PdfPropertyContext) {}

// ExitPdfProperty is called when production pdfProperty is exited.
func (s *BaseLMLListener) ExitPdfProperty(ctx *PdfPropertyContext) {}

// EnterPdfSettingsBlock is called when production pdfSettingsBlock is entered.
func (s *BaseLMLListener) EnterPdfSettingsBlock(ctx *PdfSettingsBlockContext) {}

// ExitPdfSettingsBlock is called when production pdfSettingsBlock is exited.
func (s *BaseLMLListener) ExitPdfSettingsBlock(ctx *PdfSettingsBlockContext) {}

// EnterPdfPrefaceBlock is called when production pdfPrefaceBlock is entered.
func (s *BaseLMLListener) EnterPdfPrefaceBlock(ctx *PdfPrefaceBlockContext) {}

// ExitPdfPrefaceBlock is called when production pdfPrefaceBlock is exited.
func (s *BaseLMLListener) ExitPdfPrefaceBlock(ctx *PdfPrefaceBlockContext) {}

// EnterPropertyBlock is called when production propertyBlock is entered.
func (s *BaseLMLListener) EnterPropertyBlock(ctx *PropertyBlockContext) {}

// ExitPropertyBlock is called when production propertyBlock is exited.
func (s *BaseLMLListener) ExitPropertyBlock(ctx *PropertyBlockContext) {}

// EnterSection is called when production section is entered.
func (s *BaseLMLListener) EnterSection(ctx *SectionContext) {}

// ExitSection is called when production section is exited.
func (s *BaseLMLListener) ExitSection(ctx *SectionContext) {}

// EnterSubsection is called when production subsection is entered.
func (s *BaseLMLListener) EnterSubsection(ctx *SubsectionContext) {}

// ExitSubsection is called when production subsection is exited.
func (s *BaseLMLListener) ExitSubsection(ctx *SubsectionContext) {}

// EnterSubsubsection is called when production subsubsection is entered.
func (s *BaseLMLListener) EnterSubsubsection(ctx *SubsubsectionContext) {}

// ExitSubsubsection is called when production subsubsection is exited.
func (s *BaseLMLListener) ExitSubsubsection(ctx *SubsubsectionContext) {}

// EnterStatement is called when production statement is entered.
func (s *BaseLMLListener) EnterStatement(ctx *StatementContext) {}

// ExitStatement is called when production statement is exited.
func (s *BaseLMLListener) ExitStatement(ctx *StatementContext) {}

// EnterIfStatement is called when production ifStatement is entered.
func (s *BaseLMLListener) EnterIfStatement(ctx *IfStatementContext) {}

// ExitIfStatement is called when production ifStatement is exited.
func (s *BaseLMLListener) ExitIfStatement(ctx *IfStatementContext) {}

// EnterIfClause is called when production ifClause is entered.
func (s *BaseLMLListener) EnterIfClause(ctx *IfClauseContext) {}

// ExitIfClause is called when production ifClause is exited.
func (s *BaseLMLListener) ExitIfClause(ctx *IfClauseContext) {}

// EnterElseIfClause is called when production elseIfClause is entered.
func (s *BaseLMLListener) EnterElseIfClause(ctx *ElseIfClauseContext) {}

// ExitElseIfClause is called when production elseIfClause is exited.
func (s *BaseLMLListener) ExitElseIfClause(ctx *ElseIfClauseContext) {}

// EnterElseClause is called when production elseClause is entered.
func (s *BaseLMLListener) EnterElseClause(ctx *ElseClauseContext) {}

// ExitElseClause is called when production elseClause is exited.
func (s *BaseLMLListener) ExitElseClause(ctx *ElseClauseContext) {}

// EnterHtmlBlock is called when production htmlBlock is entered.
func (s *BaseLMLListener) EnterHtmlBlock(ctx *HtmlBlockContext) {}

// ExitHtmlBlock is called when production htmlBlock is exited.
func (s *BaseLMLListener) ExitHtmlBlock(ctx *HtmlBlockContext) {}

// EnterLdp is called when production ldp is entered.
func (s *BaseLMLListener) EnterLdp(ctx *LdpContext) {}

// ExitLdp is called when production ldp is exited.
func (s *BaseLMLListener) ExitLdp(ctx *LdpContext) {}

// EnterPageBreak is called when production pageBreak is entered.
func (s *BaseLMLListener) EnterPageBreak(ctx *PageBreakContext) {}

// ExitPageBreak is called when production pageBreak is exited.
func (s *BaseLMLListener) ExitPageBreak(ctx *PageBreakContext) {}

// EnterPdfBlock is called when production pdfBlock is entered.
func (s *BaseLMLListener) EnterPdfBlock(ctx *PdfBlockContext) {}

// ExitPdfBlock is called when production pdfBlock is exited.
func (s *BaseLMLListener) ExitPdfBlock(ctx *PdfBlockContext) {}

// EnterRepeatBlock is called when production repeatBlock is entered.
func (s *BaseLMLListener) EnterRepeatBlock(ctx *RepeatBlockContext) {}

// ExitRepeatBlock is called when production repeatBlock is exited.
func (s *BaseLMLListener) ExitRepeatBlock(ctx *RepeatBlockContext) {}

// EnterRepeatTimes is called when production repeatTimes is entered.
func (s *BaseLMLListener) EnterRepeatTimes(ctx *RepeatTimesContext) {}

// ExitRepeatTimes is called when production repeatTimes is exited.
func (s *BaseLMLListener) ExitRepeatTimes(ctx *RepeatTimesContext) {}

// EnterRestoreDate is called when production restoreDate is entered.
func (s *BaseLMLListener) EnterRestoreDate(ctx *RestoreDateContext) {}

// ExitRestoreDate is called when production restoreDate is exited.
func (s *BaseLMLListener) ExitRestoreDate(ctx *RestoreDateContext) {}

// EnterRestoreMcDay is called when production restoreMcDay is entered.
func (s *BaseLMLListener) EnterRestoreMcDay(ctx *RestoreMcDayContext) {}

// ExitRestoreMcDay is called when production restoreMcDay is exited.
func (s *BaseLMLListener) ExitRestoreMcDay(ctx *RestoreMcDayContext) {}

// EnterRestoreRealm is called when production restoreRealm is entered.
func (s *BaseLMLListener) EnterRestoreRealm(ctx *RestoreRealmContext) {}

// ExitRestoreRealm is called when production restoreRealm is exited.
func (s *BaseLMLListener) ExitRestoreRealm(ctx *RestoreRealmContext) {}

// EnterRestoreVersion is called when production restoreVersion is entered.
func (s *BaseLMLListener) EnterRestoreVersion(ctx *RestoreVersionContext) {}

// ExitRestoreVersion is called when production restoreVersion is exited.
func (s *BaseLMLListener) ExitRestoreVersion(ctx *RestoreVersionContext) {}

// EnterSetMcDay is called when production setMcDay is entered.
func (s *BaseLMLListener) EnterSetMcDay(ctx *SetMcDayContext) {}

// ExitSetMcDay is called when production setMcDay is exited.
func (s *BaseLMLListener) ExitSetMcDay(ctx *SetMcDayContext) {}

// EnterSetRealm is called when production setRealm is entered.
func (s *BaseLMLListener) EnterSetRealm(ctx *SetRealmContext) {}

// ExitSetRealm is called when production setRealm is exited.
func (s *BaseLMLListener) ExitSetRealm(ctx *SetRealmContext) {}

// EnterSetVersion is called when production setVersion is entered.
func (s *BaseLMLListener) EnterSetVersion(ctx *SetVersionContext) {}

// ExitSetVersion is called when production setVersion is exited.
func (s *BaseLMLListener) ExitSetVersion(ctx *SetVersionContext) {}

// EnterSwitchStatement is called when production switchStatement is entered.
func (s *BaseLMLListener) EnterSwitchStatement(ctx *SwitchStatementContext) {}

// ExitSwitchStatement is called when production switchStatement is exited.
func (s *BaseLMLListener) ExitSwitchStatement(ctx *SwitchStatementContext) {}

// EnterSwitchLabel is called when production switchLabel is entered.
func (s *BaseLMLListener) EnterSwitchLabel(ctx *SwitchLabelContext) {}

// ExitSwitchLabel is called when production switchLabel is exited.
func (s *BaseLMLListener) ExitSwitchLabel(ctx *SwitchLabelContext) {}

// EnterDowName is called when production dowName is entered.
func (s *BaseLMLListener) EnterDowName(ctx *DowNameContext) {}

// ExitDowName is called when production dowName is exited.
func (s *BaseLMLListener) ExitDowName(ctx *DowNameContext) {}

// EnterInsert is called when production insert is entered.
func (s *BaseLMLListener) EnterInsert(ctx *InsertContext) {}

// ExitInsert is called when production insert is exited.
func (s *BaseLMLListener) ExitInsert(ctx *InsertContext) {}

// EnterAnchor is called when production anchor is entered.
func (s *BaseLMLListener) EnterAnchor(ctx *AnchorContext) {}

// ExitAnchor is called when production anchor is exited.
func (s *BaseLMLListener) ExitAnchor(ctx *AnchorContext) {}

// EnterHref is called when production href is entered.
func (s *BaseLMLListener) EnterHref(ctx *HrefContext) {}

// ExitHref is called when production href is exited.
func (s *BaseLMLListener) ExitHref(ctx *HrefContext) {}

// EnterHrefId is called when production hrefId is entered.
func (s *BaseLMLListener) EnterHrefId(ctx *HrefIdContext) {}

// ExitHrefId is called when production hrefId is exited.
func (s *BaseLMLListener) ExitHrefId(ctx *HrefIdContext) {}

// EnterTarget is called when production target is entered.
func (s *BaseLMLListener) EnterTarget(ctx *TargetContext) {}

// ExitTarget is called when production target is exited.
func (s *BaseLMLListener) ExitTarget(ctx *TargetContext) {}

// EnterTargetValue is called when production targetValue is entered.
func (s *BaseLMLListener) EnterTargetValue(ctx *TargetValueContext) {}

// ExitTargetValue is called when production targetValue is exited.
func (s *BaseLMLListener) ExitTargetValue(ctx *TargetValueContext) {}

// EnterAnchorLabel is called when production anchorLabel is entered.
func (s *BaseLMLListener) EnterAnchorLabel(ctx *AnchorLabelContext) {}

// ExitAnchorLabel is called when production anchorLabel is exited.
func (s *BaseLMLListener) ExitAnchorLabel(ctx *AnchorLabelContext) {}

// EnterAnchorLabelId is called when production anchorLabelId is entered.
func (s *BaseLMLListener) EnterAnchorLabelId(ctx *AnchorLabelIdContext) {}

// ExitAnchorLabelId is called when production anchorLabelId is exited.
func (s *BaseLMLListener) ExitAnchorLabelId(ctx *AnchorLabelIdContext) {}

// EnterImage is called when production image is entered.
func (s *BaseLMLListener) EnterImage(ctx *ImageContext) {}

// ExitImage is called when production image is exited.
func (s *BaseLMLListener) ExitImage(ctx *ImageContext) {}

// EnterSource is called when production source is entered.
func (s *BaseLMLListener) EnterSource(ctx *SourceContext) {}

// ExitSource is called when production source is exited.
func (s *BaseLMLListener) ExitSource(ctx *SourceContext) {}

// EnterTitle is called when production title is entered.
func (s *BaseLMLListener) EnterTitle(ctx *TitleContext) {}

// ExitTitle is called when production title is exited.
func (s *BaseLMLListener) ExitTitle(ctx *TitleContext) {}

// EnterWidth is called when production width is entered.
func (s *BaseLMLListener) EnterWidth(ctx *WidthContext) {}

// ExitWidth is called when production width is exited.
func (s *BaseLMLListener) ExitWidth(ctx *WidthContext) {}

// EnterHeight is called when production height is entered.
func (s *BaseLMLListener) EnterHeight(ctx *HeightContext) {}

// ExitHeight is called when production height is exited.
func (s *BaseLMLListener) ExitHeight(ctx *HeightContext) {}

// EnterH1 is called when production h1 is entered.
func (s *BaseLMLListener) EnterH1(ctx *H1Context) {}

// ExitH1 is called when production h1 is exited.
func (s *BaseLMLListener) ExitH1(ctx *H1Context) {}

// EnterH2 is called when production h2 is entered.
func (s *BaseLMLListener) EnterH2(ctx *H2Context) {}

// ExitH2 is called when production h2 is exited.
func (s *BaseLMLListener) ExitH2(ctx *H2Context) {}

// EnterH3 is called when production h3 is entered.
func (s *BaseLMLListener) EnterH3(ctx *H3Context) {}

// ExitH3 is called when production h3 is exited.
func (s *BaseLMLListener) ExitH3(ctx *H3Context) {}

// EnterPara is called when production para is entered.
func (s *BaseLMLListener) EnterPara(ctx *ParaContext) {}

// ExitPara is called when production para is exited.
func (s *BaseLMLListener) ExitPara(ctx *ParaContext) {}

// EnterPspan is called when production pspan is entered.
func (s *BaseLMLListener) EnterPspan(ctx *PspanContext) {}

// ExitPspan is called when production pspan is exited.
func (s *BaseLMLListener) ExitPspan(ctx *PspanContext) {}

// EnterSpan is called when production span is entered.
func (s *BaseLMLListener) EnterSpan(ctx *SpanContext) {}

// ExitSpan is called when production span is exited.
func (s *BaseLMLListener) ExitSpan(ctx *SpanContext) {}

// EnterMedia is called when production media is entered.
func (s *BaseLMLListener) EnterMedia(ctx *MediaContext) {}

// ExitMedia is called when production media is exited.
func (s *BaseLMLListener) ExitMedia(ctx *MediaContext) {}

// EnterNid is called when production nid is entered.
func (s *BaseLMLListener) EnterNid(ctx *NidContext) {}

// ExitNid is called when production nid is exited.
func (s *BaseLMLListener) ExitNid(ctx *NidContext) {}

// EnterLid is called when production lid is entered.
func (s *BaseLMLListener) EnterLid(ctx *LidContext) {}

// ExitLid is called when production lid is exited.
func (s *BaseLMLListener) ExitLid(ctx *LidContext) {}

// EnterMonthName is called when production monthName is entered.
func (s *BaseLMLListener) EnterMonthName(ctx *MonthNameContext) {}

// ExitMonthName is called when production monthName is exited.
func (s *BaseLMLListener) ExitMonthName(ctx *MonthNameContext) {}

// EnterLdpType is called when production ldpType is entered.
func (s *BaseLMLListener) EnterLdpType(ctx *LdpTypeContext) {}

// ExitLdpType is called when production ldpType is exited.
func (s *BaseLMLListener) ExitLdpType(ctx *LdpTypeContext) {}

// EnterPosition is called when production position is entered.
func (s *BaseLMLListener) EnterPosition(ctx *PositionContext) {}

// ExitPosition is called when production position is exited.
func (s *BaseLMLListener) ExitPosition(ctx *PositionContext) {}

// EnterPositionType is called when production positionType is entered.
func (s *BaseLMLListener) EnterPositionType(ctx *PositionTypeContext) {}

// ExitPositionType is called when production positionType is exited.
func (s *BaseLMLListener) ExitPositionType(ctx *PositionTypeContext) {}

// EnterDirective is called when production directive is entered.
func (s *BaseLMLListener) EnterDirective(ctx *DirectiveContext) {}

// ExitDirective is called when production directive is exited.
func (s *BaseLMLListener) ExitDirective(ctx *DirectiveContext) {}

// EnterLookup is called when production lookup is entered.
func (s *BaseLMLListener) EnterLookup(ctx *LookupContext) {}

// ExitLookup is called when production lookup is exited.
func (s *BaseLMLListener) ExitLookup(ctx *LookupContext) {}

// EnterInsertDate is called when production insertDate is entered.
func (s *BaseLMLListener) EnterInsertDate(ctx *InsertDateContext) {}

// ExitInsertDate is called when production insertDate is exited.
func (s *BaseLMLListener) ExitInsertDate(ctx *InsertDateContext) {}

// EnterInsertH1 is called when production insertH1 is entered.
func (s *BaseLMLListener) EnterInsertH1(ctx *InsertH1Context) {}

// ExitInsertH1 is called when production insertH1 is exited.
func (s *BaseLMLListener) ExitInsertH1(ctx *InsertH1Context) {}

// EnterInsertH2 is called when production insertH2 is entered.
func (s *BaseLMLListener) EnterInsertH2(ctx *InsertH2Context) {}

// ExitInsertH2 is called when production insertH2 is exited.
func (s *BaseLMLListener) ExitInsertH2(ctx *InsertH2Context) {}

// EnterInsertH3 is called when production insertH3 is entered.
func (s *BaseLMLListener) EnterInsertH3(ctx *InsertH3Context) {}

// ExitInsertH3 is called when production insertH3 is exited.
func (s *BaseLMLListener) ExitInsertH3(ctx *InsertH3Context) {}

// EnterRid is called when production rid is entered.
func (s *BaseLMLListener) EnterRid(ctx *RidContext) {}

// ExitRid is called when production rid is exited.
func (s *BaseLMLListener) ExitRid(ctx *RidContext) {}

// EnterOverride is called when production override is entered.
func (s *BaseLMLListener) EnterOverride(ctx *OverrideContext) {}

// ExitOverride is called when production override is exited.
func (s *BaseLMLListener) ExitOverride(ctx *OverrideContext) {}

// EnterOverrideMode is called when production overrideMode is entered.
func (s *BaseLMLListener) EnterOverrideMode(ctx *OverrideModeContext) {}

// ExitOverrideMode is called when production overrideMode is exited.
func (s *BaseLMLListener) ExitOverrideMode(ctx *OverrideModeContext) {}

// EnterOverrideDay is called when production overrideDay is entered.
func (s *BaseLMLListener) EnterOverrideDay(ctx *OverrideDayContext) {}

// ExitOverrideDay is called when production overrideDay is exited.
func (s *BaseLMLListener) ExitOverrideDay(ctx *OverrideDayContext) {}

// EnterBlankLine is called when production blankLine is entered.
func (s *BaseLMLListener) EnterBlankLine(ctx *BlankLineContext) {}

// ExitBlankLine is called when production blankLine is exited.
func (s *BaseLMLListener) ExitBlankLine(ctx *BlankLineContext) {}

// EnterHorizontalRule is called when production horizontalRule is entered.
func (s *BaseLMLListener) EnterHorizontalRule(ctx *HorizontalRuleContext) {}

// ExitHorizontalRule is called when production horizontalRule is exited.
func (s *BaseLMLListener) ExitHorizontalRule(ctx *HorizontalRuleContext) {}

// EnterSid is called when production sid is entered.
func (s *BaseLMLListener) EnterSid(ctx *SidContext) {}

// ExitSid is called when production sid is exited.
func (s *BaseLMLListener) ExitSid(ctx *SidContext) {}

// EnterTmplDate is called when production tmplDate is entered.
func (s *BaseLMLListener) EnterTmplDate(ctx *TmplDateContext) {}

// ExitTmplDate is called when production tmplDate is exited.
func (s *BaseLMLListener) ExitTmplDate(ctx *TmplDateContext) {}

// EnterTmplCalendar is called when production tmplCalendar is entered.
func (s *BaseLMLListener) EnterTmplCalendar(ctx *TmplCalendarContext) {}

// ExitTmplCalendar is called when production tmplCalendar is exited.
func (s *BaseLMLListener) ExitTmplCalendar(ctx *TmplCalendarContext) {}

// EnterTmplCss is called when production tmplCss is entered.
func (s *BaseLMLListener) EnterTmplCss(ctx *TmplCssContext) {}

// ExitTmplCss is called when production tmplCss is exited.
func (s *BaseLMLListener) ExitTmplCss(ctx *TmplCssContext) {}

// EnterTmplDay is called when production tmplDay is entered.
func (s *BaseLMLListener) EnterTmplDay(ctx *TmplDayContext) {}

// ExitTmplDay is called when production tmplDay is exited.
func (s *BaseLMLListener) ExitTmplDay(ctx *TmplDayContext) {}

// EnterTmplID is called when production tmplID is entered.
func (s *BaseLMLListener) EnterTmplID(ctx *TmplIDContext) {}

// ExitTmplID is called when production tmplID is exited.
func (s *BaseLMLListener) ExitTmplID(ctx *TmplIDContext) {}

// EnterTmplModel is called when production tmplModel is entered.
func (s *BaseLMLListener) EnterTmplModel(ctx *TmplModelContext) {}

// ExitTmplModel is called when production tmplModel is exited.
func (s *BaseLMLListener) ExitTmplModel(ctx *TmplModelContext) {}

// EnterTmplMonth is called when production tmplMonth is entered.
func (s *BaseLMLListener) EnterTmplMonth(ctx *TmplMonthContext) {}

// ExitTmplMonth is called when production tmplMonth is exited.
func (s *BaseLMLListener) ExitTmplMonth(ctx *TmplMonthContext) {}

// EnterTmplOutput is called when production tmplOutput is entered.
func (s *BaseLMLListener) EnterTmplOutput(ctx *TmplOutputContext) {}

// ExitTmplOutput is called when production tmplOutput is exited.
func (s *BaseLMLListener) ExitTmplOutput(ctx *TmplOutputContext) {}

// EnterTmplPageHeaderEven is called when production tmplPageHeaderEven is entered.
func (s *BaseLMLListener) EnterTmplPageHeaderEven(ctx *TmplPageHeaderEvenContext) {}

// ExitTmplPageHeaderEven is called when production tmplPageHeaderEven is exited.
func (s *BaseLMLListener) ExitTmplPageHeaderEven(ctx *TmplPageHeaderEvenContext) {}

// EnterTmplPageFooterEven is called when production tmplPageFooterEven is entered.
func (s *BaseLMLListener) EnterTmplPageFooterEven(ctx *TmplPageFooterEvenContext) {}

// ExitTmplPageFooterEven is called when production tmplPageFooterEven is exited.
func (s *BaseLMLListener) ExitTmplPageFooterEven(ctx *TmplPageFooterEvenContext) {}

// EnterTmplPageHeaderFirst is called when production tmplPageHeaderFirst is entered.
func (s *BaseLMLListener) EnterTmplPageHeaderFirst(ctx *TmplPageHeaderFirstContext) {}

// ExitTmplPageHeaderFirst is called when production tmplPageHeaderFirst is exited.
func (s *BaseLMLListener) ExitTmplPageHeaderFirst(ctx *TmplPageHeaderFirstContext) {}

// EnterTmplPageFooterFirst is called when production tmplPageFooterFirst is entered.
func (s *BaseLMLListener) EnterTmplPageFooterFirst(ctx *TmplPageFooterFirstContext) {}

// ExitTmplPageFooterFirst is called when production tmplPageFooterFirst is exited.
func (s *BaseLMLListener) ExitTmplPageFooterFirst(ctx *TmplPageFooterFirstContext) {}

// EnterTmplPageHeaderOdd is called when production tmplPageHeaderOdd is entered.
func (s *BaseLMLListener) EnterTmplPageHeaderOdd(ctx *TmplPageHeaderOddContext) {}

// ExitTmplPageHeaderOdd is called when production tmplPageHeaderOdd is exited.
func (s *BaseLMLListener) ExitTmplPageHeaderOdd(ctx *TmplPageHeaderOddContext) {}

// EnterTmplPageFooterOdd is called when production tmplPageFooterOdd is entered.
func (s *BaseLMLListener) EnterTmplPageFooterOdd(ctx *TmplPageFooterOddContext) {}

// ExitTmplPageFooterOdd is called when production tmplPageFooterOdd is exited.
func (s *BaseLMLListener) ExitTmplPageFooterOdd(ctx *TmplPageFooterOddContext) {}

// EnterTmplPageHeader is called when production tmplPageHeader is entered.
func (s *BaseLMLListener) EnterTmplPageHeader(ctx *TmplPageHeaderContext) {}

// ExitTmplPageHeader is called when production tmplPageHeader is exited.
func (s *BaseLMLListener) ExitTmplPageHeader(ctx *TmplPageHeaderContext) {}

// EnterTmplPageFooter is called when production tmplPageFooter is entered.
func (s *BaseLMLListener) EnterTmplPageFooter(ctx *TmplPageFooterContext) {}

// ExitTmplPageFooter is called when production tmplPageFooter is exited.
func (s *BaseLMLListener) ExitTmplPageFooter(ctx *TmplPageFooterContext) {}

// EnterTmplPageNumber is called when production tmplPageNumber is entered.
func (s *BaseLMLListener) EnterTmplPageNumber(ctx *TmplPageNumberContext) {}

// ExitTmplPageNumber is called when production tmplPageNumber is exited.
func (s *BaseLMLListener) ExitTmplPageNumber(ctx *TmplPageNumberContext) {}

// EnterTmplStatus is called when production tmplStatus is entered.
func (s *BaseLMLListener) EnterTmplStatus(ctx *TmplStatusContext) {}

// ExitTmplStatus is called when production tmplStatus is exited.
func (s *BaseLMLListener) ExitTmplStatus(ctx *TmplStatusContext) {}

// EnterTmplIndexLastTitleOverride is called when production tmplIndexLastTitleOverride is entered.
func (s *BaseLMLListener) EnterTmplIndexLastTitleOverride(ctx *TmplIndexLastTitleOverrideContext) {}

// ExitTmplIndexLastTitleOverride is called when production tmplIndexLastTitleOverride is exited.
func (s *BaseLMLListener) ExitTmplIndexLastTitleOverride(ctx *TmplIndexLastTitleOverrideContext) {}

// EnterTmplTitleCodes is called when production tmplTitleCodes is entered.
func (s *BaseLMLListener) EnterTmplTitleCodes(ctx *TmplTitleCodesContext) {}

// ExitTmplTitleCodes is called when production tmplTitleCodes is exited.
func (s *BaseLMLListener) ExitTmplTitleCodes(ctx *TmplTitleCodesContext) {}

// EnterTmplType is called when production tmplType is entered.
func (s *BaseLMLListener) EnterTmplType(ctx *TmplTypeContext) {}

// ExitTmplType is called when production tmplType is exited.
func (s *BaseLMLListener) ExitTmplType(ctx *TmplTypeContext) {}

// EnterTmplOffice is called when production tmplOffice is entered.
func (s *BaseLMLListener) EnterTmplOffice(ctx *TmplOfficeContext) {}

// ExitTmplOffice is called when production tmplOffice is exited.
func (s *BaseLMLListener) ExitTmplOffice(ctx *TmplOfficeContext) {}

// EnterTmplYear is called when production tmplYear is entered.
func (s *BaseLMLListener) EnterTmplYear(ctx *TmplYearContext) {}

// ExitTmplYear is called when production tmplYear is exited.
func (s *BaseLMLListener) ExitTmplYear(ctx *TmplYearContext) {}

// EnterBlock is called when production block is entered.
func (s *BaseLMLListener) EnterBlock(ctx *BlockContext) {}

// ExitBlock is called when production block is exited.
func (s *BaseLMLListener) ExitBlock(ctx *BlockContext) {}

// EnterCaseStatement is called when production caseStatement is entered.
func (s *BaseLMLListener) EnterCaseStatement(ctx *CaseStatementContext) {}

// ExitCaseStatement is called when production caseStatement is exited.
func (s *BaseLMLListener) ExitCaseStatement(ctx *CaseStatementContext) {}

// EnterSwitchDefault is called when production switchDefault is entered.
func (s *BaseLMLListener) EnterSwitchDefault(ctx *SwitchDefaultContext) {}

// ExitSwitchDefault is called when production switchDefault is exited.
func (s *BaseLMLListener) ExitSwitchDefault(ctx *SwitchDefaultContext) {}

// EnterCaseExpression is called when production caseExpression is entered.
func (s *BaseLMLListener) EnterCaseExpression(ctx *CaseExpressionContext) {}

// ExitCaseExpression is called when production caseExpression is exited.
func (s *BaseLMLListener) ExitCaseExpression(ctx *CaseExpressionContext) {}

// EnterIntegerExpression is called when production integerExpression is entered.
func (s *BaseLMLListener) EnterIntegerExpression(ctx *IntegerExpressionContext) {}

// ExitIntegerExpression is called when production integerExpression is exited.
func (s *BaseLMLListener) ExitIntegerExpression(ctx *IntegerExpressionContext) {}

// EnterIntegerList is called when production integerList is entered.
func (s *BaseLMLListener) EnterIntegerList(ctx *IntegerListContext) {}

// ExitIntegerList is called when production integerList is exited.
func (s *BaseLMLListener) ExitIntegerList(ctx *IntegerListContext) {}

// EnterDowExpression is called when production dowExpression is entered.
func (s *BaseLMLListener) EnterDowExpression(ctx *DowExpressionContext) {}

// ExitDowExpression is called when production dowExpression is exited.
func (s *BaseLMLListener) ExitDowExpression(ctx *DowExpressionContext) {}

// EnterRidExpression is called when production ridExpression is entered.
func (s *BaseLMLListener) EnterRidExpression(ctx *RidExpressionContext) {}

// ExitRidExpression is called when production ridExpression is exited.
func (s *BaseLMLListener) ExitRidExpression(ctx *RidExpressionContext) {}

// EnterDowList is called when production dowList is entered.
func (s *BaseLMLListener) EnterDowList(ctx *DowListContext) {}

// ExitDowList is called when production dowList is exited.
func (s *BaseLMLListener) ExitDowList(ctx *DowListContext) {}

// EnterMonthDay is called when production monthDay is entered.
func (s *BaseLMLListener) EnterMonthDay(ctx *MonthDayContext) {}

// ExitMonthDay is called when production monthDay is exited.
func (s *BaseLMLListener) ExitMonthDay(ctx *MonthDayContext) {}

// EnterOperator is called when production operator is entered.
func (s *BaseLMLListener) EnterOperator(ctx *OperatorContext) {}

// ExitOperator is called when production operator is exited.
func (s *BaseLMLListener) ExitOperator(ctx *OperatorContext) {}

// EnterExpression is called when production expression is entered.
func (s *BaseLMLListener) EnterExpression(ctx *ExpressionContext) {}

// ExitExpression is called when production expression is exited.
func (s *BaseLMLListener) ExitExpression(ctx *ExpressionContext) {}
